-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-12-2018 a las 06:34:30
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_planilla`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_empleo`
--

CREATE TABLE `datos_empleo` (
  `id_datos` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `pagohora` double NOT NULL,
  `thoras` int(5) NOT NULL,
  `tipopago` varchar(45) NOT NULL,
  `rap` int(1) NOT NULL,
  `ihss` int(1) NOT NULL,
  `coop` int(1) NOT NULL,
  `otro` int(1) NOT NULL,
  `id_depto` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datos_empleo`
--

INSERT INTO `datos_empleo` (`id_datos`, `id_empleado`, `pagohora`, `thoras`, `tipopago`, `rap`, `ihss`, `coop`, `otro`, `id_depto`) VALUES
(1, 3, 52, 8, 'Hora', 1, 1, 1, 1, 3),
(2, 4, 52, 8, 'Quincenal', 1, 1, 1, 1, 3),
(3, 5, 52, 8, '', 0, 0, 0, 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deptos`
--

CREATE TABLE `deptos` (
  `id_depto` int(11) NOT NULL,
  `depto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `deptos`
--

INSERT INTO `deptos` (`id_depto`, `depto`) VALUES
(1, 'SECRETARIA'),
(2, 'LIMPIEZA'),
(3, 'GERENTE'),
(4, 'EDUCACION');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id_empleado` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `nombre2` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `apellido2` varchar(45) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `identidad` varchar(15) NOT NULL,
  `rtn` varchar(15) NOT NULL,
  `fecha` datetime NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id_empleado`, `nombre`, `nombre2`, `apellido`, `apellido2`, `celular`, `telefono`, `identidad`, `rtn`, `fecha`, `email`) VALUES
(5, 'Christian', '----', 'Lopez', '---', '5448798', '989898', '010459849', '95964646', '0000-00-00 00:00:00', 'ninguno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id_pago` int(11) NOT NULL,
  `id_empleado` int(5) NOT NULL,
  `id_datos` int(5) NOT NULL,
  `horastrab` double NOT NULL,
  `horasext` double NOT NULL,
  `rap` double NOT NULL,
  `ihss` double NOT NULL,
  `coop` double NOT NULL,
  `otro` double NOT NULL,
  `totaldeduccion` double NOT NULL,
  `pagototal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos_empleo`
--
ALTER TABLE `datos_empleo`
  ADD PRIMARY KEY (`id_datos`);

--
-- Indices de la tabla `deptos`
--
ALTER TABLE `deptos`
  ADD PRIMARY KEY (`id_depto`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_empleado`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id_pago`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `datos_empleo`
--
ALTER TABLE `datos_empleo`
  MODIFY `id_datos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `deptos`
--
ALTER TABLE `deptos`
  MODIFY `id_depto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `id_pago` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
